#include <Elementary.h>
#include <Elementary_Cursor.h>
#include "extra_private.h"
#include "../lib/extra.h"

static Evas_Object* extra_theme_small_new(Evas_Object *par, Extra_Theme *theme);

static Extra_Ui_Small_Preview_Accessor acc = {
  ((Extra_ui_preview_get*) extra_theme_preview_get),
  ((Extra_ui_preview_download*) extra_theme_preview_download),
};

static void
_install_done(void *data)
{
   Extra_Theme *theme = data;

   if (extra_theme_installed(theme))
     extra_ui_theme_ask_for_default(theme);

   elm_gengrid_clear(_ui.theme_selector);
   extra_ui_site_refill(_ui.theme_selector);
   extra_ui_show_popup_toolbar(NULL);
}

static void
_install_theme(void *data, Evas_Object *obj EINA_UNUSED, void *event_info EINA_UNUSED)
{
   Extra_Theme *candidate = data;
   Extra_Progress *progress;
   Eina_Strbuf *title;

   title = eina_strbuf_new();
   eina_strbuf_append_printf(title, _("Installing theme %s!"), candidate->obj.name);

   progress = extra_ui_progress_popup_show(eina_strbuf_string_get(title), _install_done, candidate);
   extra_theme_download(progress, candidate);
}


static void
_set_as_default(void *data, Evas_Object *obj EINA_UNUSED, void *event_info EINA_UNUSED)
{
   Extra_Theme *candidate = data;

   extra_theme_use(candidate);
   gengrid_reapply_state(_ui.theme_selector);
}

static void
_back_to_default(void *data EINA_UNUSED, Evas_Object *obj EINA_UNUSED, void *event_info EINA_UNUSED)
{
   extra_theme_reset();
   gengrid_reapply_state(_ui.theme_selector);
}

static Evas_Object*
_action_button_create(Evas_Object *par, Extra_Theme *theme)
{
   Evas_Object *install = NULL, *icon = NULL;

   icon = elm_icon_add(par);
   evas_object_show(icon);

   install = elm_button_add(par);
   elm_object_part_content_set(install, "icon", icon);
   evas_object_size_hint_weight_set(install, EVAS_HINT_EXPAND, 0.0);
   evas_object_size_hint_align_set(install, 1.0, 0.5);
   evas_object_show(install);

   if (!extra_theme_installed(theme) && extra_theme_installed_old(theme))
     {
        //there is a update available add a update button
        elm_icon_standard_set(icon, "software-update-available");
        evas_object_smart_callback_add(install, "clicked", _install_theme, theme);
        elm_object_text_set(install, _("Update"));
     }
   else if (!extra_theme_installed(theme))
     {
        //that is not installed add a install utton
        elm_icon_standard_set(icon, "emblem-downloads");
        evas_object_smart_callback_add(install, "clicked", _install_theme, theme);
        elm_object_text_set(install, _("Install"));
     }
   else if (extra_theme_installed(theme) && !extra_theme_default_get(theme))
     {
        //the theme is installed but not the default
        elm_icon_standard_set(icon, "emblem-favorite");
        evas_object_smart_callback_add(install, "clicked", _set_as_default, theme);
        elm_object_text_set(install, _("Set as default"));
     }
   else if (extra_theme_installed(theme) && extra_theme_default_get(theme))
     {
        //the theme is installed but not the default
        elm_icon_standard_set(icon, "emblem-favorite");
        evas_object_smart_callback_add(install, "clicked", _back_to_default, theme);
        elm_object_text_set(install, _("Back to default"));
     }
   return install;
}

static void
_popup_theme(Extra_Theme *theme)
{
   Evas_Object *o, *table;

   table = extra_ui_base_object_detail(theme, &theme->obj, acc, _ui.win);

   o = elm_entry_add(table);
   elm_entry_editable_set(o, EINA_FALSE);
   elm_object_text_set(o, theme->description);
   evas_object_size_hint_weight_set(o, EVAS_HINT_EXPAND, 0.0);
   evas_object_size_hint_align_set(o, EVAS_HINT_FILL, EVAS_HINT_FILL);
   evas_object_show(o);
   elm_table_pack(table, o, 0, 3, 2, 2);

   extra_ui_details_window_show(table);

}

static Evas_Object*
_content_basic_get(void *data, Evas_Object *obj, const char *source)
{
   if (!strcmp(source, "elm.swallow.end"))
     return NULL;


   return extra_theme_small_new(obj, data);
}

static char*
_text_basic_get(void *data, Evas_Object *obj EINA_UNUSED, const char *source EINA_UNUSED)
{
   Extra_Theme *t = data;

   return strdup(t->obj.name);
}

static void
_show_details(void *data, Evas_Object *obj EINA_UNUSED, void *event_info EINA_UNUSED)
{
   _popup_theme(data);
   evas_object_del(_ui.popup_toolbar);
}

static void
_show_fullscreen(void *data, Evas_Object *obj EINA_UNUSED, void *event_info EINA_UNUSED)
{
   char *path;

   path = extra_theme_preview_get(data);
   if (!path) return;

   extra_ui_fullscreen_preview(path);
}

static void
_item_selected(void *data EINA_UNUSED, Evas_Object *obj EINA_UNUSED, void *event_info EINA_UNUSED)
{
   Evas_Object *box, *o, *icon;
   Elm_Gengrid_Item *it = event_info;
   Extra_Theme *t = elm_object_item_data_get(it);

   box = elm_box_add(_ui.win);
   elm_box_horizontal_set(box, EINA_TRUE);

   o = _action_button_create(box, t);
   if (o)
     elm_box_pack_end(box, o);

   o = elm_button_add(box);
   evas_object_smart_callback_add(o, "clicked", _show_fullscreen, t);
   elm_object_text_set(o, _("Show fullscreen"));
   evas_object_show(o);
   elm_box_pack_end(box, o);
   icon = elm_icon_add(o);
   elm_icon_standard_set(icon, "view-fullscreen");
   elm_object_part_content_set(o, "icon", icon);

   o = elm_button_add(box);
   evas_object_smart_callback_add(o, "clicked", _show_details, t);
   elm_object_text_set(o, _("Show Details"));
   evas_object_show(o);
   elm_box_pack_end(box, o);
   icon = elm_icon_add(o);
   elm_icon_standard_set(icon, "document-properties");
   elm_object_part_content_set(o, "icon", icon);

   extra_ui_show_popup_toolbar(box);
}

//==== Small preview

static Evas_Object*
_icon_add(Evas_Object *obj, const char *name, const char *desc)
{
   Evas_Object *ret;

   ret = elm_icon_add(obj);
   evas_object_size_hint_min_set(ret, 20, 20);
   elm_icon_standard_set(ret, name);
   evas_object_show(ret);

   elm_object_tooltip_text_set(ret, desc);
   elm_object_tooltip_orient_set(ret, ELM_TOOLTIP_ORIENT_TOP);

   return ret;
}

static Evas_Object*
extra_theme_small_new(Evas_Object *par, Extra_Theme *theme)
{
   Evas_Object *table, *o;
   int counter = 0;

   table = extra_ui_small_preview_new(acc, par, theme);

   //for available states: downloaded, set as default, new-version available
   if (extra_theme_installed(theme))
     {
        //theme is installed
        o = _icon_add(par, "emblem-downloads", _("This theme is installed"));
        elm_table_pack(table, o, counter, 0, 1, 1);
        counter ++;
     }

   if (extra_theme_default_get(theme))
     {
        //theme is default
        o = _icon_add(par, "emblem-default", _("This theme is set as default"));
        elm_table_pack(table, o, counter, 0, 1, 1);
        counter ++;
     }

   if (!extra_theme_installed(theme) && extra_theme_installed_old(theme))
     {
        o = _icon_add(par, "software-update-available", _("There are updates available"));
        elm_table_pack(table, o, counter, 0, 1, 1);
        counter ++;
     }
   return table;
}

//external accessable

Extra_Ui_Site_Template theme_site = {
  extra_themes_list,
  "thumb",
  _content_basic_get,
  _text_basic_get,
  _item_selected,
};
