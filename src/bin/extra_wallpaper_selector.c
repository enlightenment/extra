#include <Elementary.h>
#include <Elementary_Cursor.h>
#include "extra_private.h"
#include "../lib/extra.h"

static Extra_Ui_Small_Preview_Accessor acc = {
    ((Extra_ui_preview_get*) extra_wallpaper_preview_get),
    ((Extra_ui_preview_download*) extra_wallpaper_preview_download),
};


static void
_wallpaper_installed(void *data EINA_UNUSED)
{
    /*TODO FIND A WAY TO REMOTE OPEN ENLIGHTENMENT BACKGROUND SELECTOR*/
    gengrid_reapply_state(_ui.wallpaper_selector);
}

static void
_install_wallpaper(void *data, Evas_Object *obj EINA_UNUSED, void *event_info EINA_UNUSED)
{
   Extra_Wallpaper *b = data;
   Extra_Progress *p;

   p = extra_ui_progress_popup_show(_("Installing wallpaper"), _wallpaper_installed, b);
   extra_wallpaper_download(p, b);
}

static void
_uninstall_wallpaper(void *data, Evas_Object *obj EINA_UNUSED, void *event_info EINA_UNUSED)
{
   Extra_Wallpaper *b = data;

   extra_wallpaper_delete(b);
   gengrid_reapply_state(_ui.wallpaper_selector);
}

static void
_fullscreen_wallpaper(void *data, Evas_Object *obj EINA_UNUSED, void *event_info EINA_UNUSED)
{
   Extra_Wallpaper *b = data;
   char *path;

   path = extra_wallpaper_preview_get(b);
   if (!path) return;

   extra_ui_fullscreen_preview(path);
}

static void
_show_details(void *data, Evas_Object *obj EINA_UNUSED, void *event_info EINA_UNUSED)
{
   Extra_Wallpaper *wallpaper = data;
   Evas_Object *o;

   o = extra_ui_base_object_detail(wallpaper, &wallpaper->obj, acc, _ui.win);
   extra_ui_details_window_show(o);

   evas_object_del(_ui.popup_toolbar);
}

static void
_show_tooltip(Extra_Wallpaper *b)
{
    Evas_Object *box, *o, *icon;

    box = elm_box_add(_ui.win);
    elm_box_horizontal_set(box, EINA_TRUE);

    o = elm_button_add(box);
    icon = elm_icon_add(o);

    if (extra_wallpaper_installed(b))
      {
         elm_object_text_set(o, _("Uninstall"));
         elm_icon_standard_set(icon, "edit-delete");
         evas_object_smart_callback_add(o, "clicked", _uninstall_wallpaper, b);
      }
    else
      {
         elm_object_text_set(o, _("Install"));
         elm_icon_standard_set(icon, "emblem-downloads");
         evas_object_smart_callback_add(o, "clicked", _install_wallpaper, b);
      }
    elm_object_part_content_set(o, "icon", icon);
    elm_box_pack_end(box, o);
    evas_object_show(o);

    //view-fullscreen
    o = elm_button_add(box);
    elm_object_text_set(o, _("Show fullscreen"));
    evas_object_smart_callback_add(o, "clicked", _fullscreen_wallpaper , b);
    elm_box_pack_end(box, o);
    evas_object_show(o);
    icon = elm_icon_add(o);
    elm_icon_standard_set(icon, "view-fullscreen");
    elm_object_part_content_set(o, "icon", icon);

    //show details
    o = elm_button_add(box);
    evas_object_smart_callback_add(o, "clicked", _show_details, b);
    elm_object_text_set(o, _("Show Details"));
    evas_object_show(o);
    elm_box_pack_end(box, o);
    icon = elm_icon_add(o);
    elm_icon_standard_set(icon, "document-properties");
    elm_object_part_content_set(o, "icon", icon);

    extra_ui_show_popup_toolbar(box);
}

static Evas_Object*
_content_basic_get(void *data, Evas_Object *obj, const char *source)
{
   if (!strcmp(source, "elm.swallow.end"))
     return NULL;

   return extra_ui_small_preview_new(acc, obj, data);
}

static char*
_text_basic_get(void *data, Evas_Object *obj EINA_UNUSED, const char *source EINA_UNUSED)
{
   Extra_Wallpaper *t = data;

   return strdup(t->obj.name);
}

static void
_item_selected(void *data EINA_UNUSED, Evas_Object *obj EINA_UNUSED, void *event_info)
{

   Elm_Widget_Item *it = event_info;
   _show_tooltip(elm_object_item_data_get(it));
}

Extra_Ui_Site_Template wallpaper_site = {
  extra_wallpapers_list,
  "thumb",
  _content_basic_get,
  _text_basic_get,
  _item_selected,
};
