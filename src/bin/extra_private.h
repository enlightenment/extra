#ifndef EXTRA_PRIVATE_H_
# define EXTRA_PRIVATE_H_

#include "../lib/extra.h"
#include "extra_util.h"

#define _(str) gettext(str)
#define d_(str, dom) dgettext(PACKAGE dom, str)
#define P_(str, str_p, n) ngettext(str, str_p, n)
#define dP_(str, str_p, n, dom) dngettext(PACKAGE dom, str, str_p, n)

typedef struct {
   Evas_Object *win;
   Evas_Object *wallpaper_selector;
   Evas_Object *theme_selector;
   Evas_Object *popup_toolbar;
} Ui;

extern Ui _ui;
extern Extra_Ui_Site_Template theme_site;
extern Extra_Ui_Site_Template wallpaper_site;

#endif
