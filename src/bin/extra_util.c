#include <Elementary.h>
#include "extra_private.h"
#include "../lib/extra.h"


void
gengrid_reapply_state(Evas_Object *grid)
{
    Elm_Object_Item *item;

    item = elm_gengrid_selected_item_get(grid);
    elm_gengrid_item_selected_set(item, EINA_FALSE);
    elm_gengrid_item_selected_set(item, EINA_TRUE);
}

void
extra_win_progress_popup_show(const char *title)
{
   Evas_Object *progress, *sync_popup;

   sync_popup = elm_popup_add(_ui.win);
   progress = elm_progressbar_add(sync_popup);
   elm_progressbar_pulse_set(progress, EINA_TRUE);
   elm_progressbar_pulse(progress, EINA_TRUE);
   evas_object_show(progress);

   elm_object_part_text_set(sync_popup, "title,text", title);
   elm_object_content_set(sync_popup, progress);
   evas_object_show(sync_popup);
}

static void
_extra_win_ask_yes(void *data,
                   Evas_Object *obj EINA_UNUSED,
                   void *event_info EINA_UNUSED)
{
   Extra_Theme *theme = data;
   Evas_Object *popup = evas_object_data_get(obj, "popup");

   extra_theme_use(theme);
   evas_object_del(popup);
}

static void
_extra_win_ask_no(void *data EINA_UNUSED,
                           Evas_Object *obj EINA_UNUSED,
                           void *event_info EINA_UNUSED)
{
   Evas_Object *popup = evas_object_data_get(obj, "popup");

   evas_object_del(popup);
}

void
extra_ui_theme_ask_for_default(Extra_Theme *theme)
{
   Evas_Object *o, *table, *popup;

   popup = elm_popup_add(_ui.win);

   table = elm_table_add(popup);
   elm_object_content_set(popup, table);
   evas_object_show(table);

   o = elm_label_add(popup);
   elm_object_text_set(o, _("Set the theme as default ?"));
   elm_table_pack(table, o, 0, 0, 2, 1);
   evas_object_show(o);

   o = elm_button_add(popup);
   elm_object_text_set(o, _("Yes"));
   elm_table_pack(table, o, 0, 1, 1, 1);
   evas_object_data_set(o, "popup", popup);
   evas_object_smart_callback_add(o, "clicked", _extra_win_ask_yes, theme);
   evas_object_show(o);

   o = elm_button_add(popup);
   elm_object_text_set(o, _("No"));
   elm_table_pack(table, o, 1, 1, 1, 1);
   evas_object_smart_callback_add(o, "clicked", _extra_win_ask_no, NULL);
   evas_object_data_set(o, "popup", popup);
   evas_object_show(o);

   evas_object_show(popup);
}

typedef struct {
   Evas_Object *popup, *indicator;
   Extra_Progress progress;
   Extra_Done_Cb done_cb;
   void *data;
} Extra_Ui_Progress;

static void
_extra_win_progress_popup_cb(void *data, double progress)
{
   Extra_Ui_Progress *ui = data;

   if (elm_progressbar_pulse_get(ui->indicator))
     {
        elm_progressbar_pulse(ui->indicator, EINA_FALSE);
        elm_progressbar_pulse_set(ui->indicator, EINA_FALSE);
     }

   elm_progressbar_value_set(ui->indicator, progress);
}
static void
_popup_show_done_cb(void *data)
{
   Extra_Ui_Progress *ui = data;

   evas_object_del(ui->popup);

   if (ui->done_cb)
     ui->done_cb(ui->data);

   free(ui);
}

Extra_Progress*
extra_ui_progress_popup_show(const char *title, Extra_Done_Cb done, void *data)
{
   Extra_Ui_Progress *ui = calloc(1, sizeof(Extra_Ui_Progress));

   ui->done_cb = done;
   ui->data = data;
   ui->progress.done_cb = _popup_show_done_cb;
   ui->progress.progress_cb = _extra_win_progress_popup_cb;
   ui->progress.data = ui;

   ui->popup = elm_popup_add(_ui.win);
   ui->indicator = elm_progressbar_add(ui->popup);
   elm_progressbar_pulse_set(ui->indicator, EINA_TRUE);
   elm_progressbar_pulse(ui->indicator, EINA_TRUE);
   evas_object_show(ui->indicator);

   elm_object_part_text_set(ui->popup, "title,text", title);
   elm_object_content_set(ui->popup, ui->indicator);
   evas_object_show(ui->popup);

   return &ui->progress;
}

typedef struct {
    Evas_Object *image;
    Evas_Object *progress;
    Extra_Progress p;
    void *data;
    Extra_Ui_Small_Preview_Accessor acc;
    Extra_Request *req;
} Small_Preview;

static void
_small_preview_progress_cb(void *data, double progress)
{
   Small_Preview *p = data;

   elm_progressbar_value_set(p->progress, progress);
}

static void
_small_preview_done_cb(void *data)
{
   Small_Preview *p = data;
   char *preview;

   preview = p->acc.preview_get(p->data);
   if (preview)
     {
        if (!elm_image_file_set(p->image , preview, NULL))
          {
             //the download failed, do nothing here, we likly will run into the same problem. And we should not spam our download requests
             ecore_file_remove(preview);
          }
        evas_object_show(p->image);
        free(preview);
     }
   p->req = NULL;
   evas_object_show(p->image);
   evas_object_hide(p->progress);
}

static void
_small_preview_deleted(void *data, Evas *e EINA_UNUSED, Evas_Object *obj EINA_UNUSED, void *event_info EINA_UNUSED)
{
   Small_Preview *small;

   small = data;

   extra_request_mute(small->req);

   free(small);
}

Evas_Object*
extra_ui_small_preview_new(Extra_Ui_Small_Preview_Accessor acc, Evas_Object *par, void *data)
{
   Evas_Object *table;
   Small_Preview *small;
   char *preview;

   small = calloc(1, sizeof(Small_Preview));

   small->acc = acc;
   small->data = data;
   table = elm_table_add(par);
   evas_object_event_callback_add(table, EVAS_CALLBACK_DEL, _small_preview_deleted, small);
   evas_object_size_hint_weight_set(table, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
   evas_object_size_hint_align_set(table, EVAS_HINT_FILL, EVAS_HINT_FILL);
   evas_object_show(table);


   small->image = elm_image_add(table);
   evas_object_size_hint_weight_set(small->image, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
   evas_object_size_hint_align_set(small->image, EVAS_HINT_FILL, EVAS_HINT_FILL);
   elm_table_pack(table, small->image, 0, 0, 4, 3);
   evas_object_show(small->image);

   small->progress = elm_progressbar_add(table);
   evas_object_size_hint_weight_set(small->progress, EVAS_HINT_EXPAND, 0.0);
   evas_object_size_hint_align_set(small->progress, EVAS_HINT_FILL, 0.0);
   elm_table_pack(table, small->progress, 0, 2, 4, 1);

   elm_progressbar_value_set(small->progress, 0.0);

   preview = acc.preview_get(data);
   if (preview)
     {
        if (!elm_image_file_set(small->image, preview, NULL))
          {
             ecore_file_remove(preview);
             free(preview);
             preview = NULL;
          }
        else
          {
             free(preview);
          }
     }

   if (!preview)
     {
        small->p.data = small;
        small->p.done_cb = _small_preview_done_cb;
        small->p.progress_cb = _small_preview_progress_cb;
        small->req = acc.preview_download(&small->p, data);
        evas_object_hide(small->image);
        evas_object_show(small->progress);
     }
   return table;
}

static void
_delete_inwin(void *data, Evas *e EINA_UNUSED,  Evas_Object *obj EINA_UNUSED, void *event_info)
{
   Evas_Event_Mouse_Up *ev = event_info;

   if (ev->event_flags & EVAS_EVENT_FLAG_ON_HOLD) return;

   evas_object_del(data);

   ev->event_flags |= EVAS_EVENT_FLAG_ON_HOLD;
}

static void
_key_down_cb(void *data, Evas *e EINA_UNUSED,  Evas_Object *obj EINA_UNUSED, void *event_info)
{
   Evas_Event_Key_Down *down = event_info;

   if (!strcmp(down->keyname , "Escape"))
     evas_object_del(data);

   down->event_flags |= EVAS_EVENT_FLAG_ON_HOLD;
}

void
extra_ui_fullscreen_preview(char *path)
{
   Evas_Object *win, *image;

   win = elm_win_util_standard_add("Extra - Screenshot", "Extra - Screenshot");
   evas_object_event_callback_add(win, EVAS_CALLBACK_MOUSE_UP, _delete_inwin, win);
   evas_object_event_callback_add(win, EVAS_CALLBACK_KEY_DOWN, _key_down_cb, win);
   elm_win_borderless_set(win, EINA_TRUE);
   elm_win_fullscreen_set(win, EINA_TRUE);
   elm_win_maximized_set(win, EINA_TRUE);
   evas_object_size_hint_weight_set(win, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
   evas_object_size_hint_align_set(win, EVAS_HINT_FILL, EVAS_HINT_FILL);

   image = elm_image_add(win);

   evas_object_size_hint_weight_set(image, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
   evas_object_size_hint_align_set(image, EVAS_HINT_FILL, EVAS_HINT_FILL);
   elm_image_file_set(image, path, NULL);
   evas_object_show(image);

   elm_object_content_set(win, image);

   evas_object_show(win);
}

static void
_popup_block_click_cb(void *data EINA_UNUSED, Evas *e EINA_UNUSED, Evas_Object *obj EINA_UNUSED, void *event_info EINA_UNUSED)
{
   elm_popup_dismiss(_ui.popup_toolbar);
   //from this time on we are not interested in the reference anymore
   _ui.popup_toolbar = NULL;
}

static void
_dismissed(void *data, Evas_Object *obj EINA_UNUSED, void *event_info EINA_UNUSED)
{
   evas_object_del(data);
}

static void
_popup_del_cb(void *data, Evas *e EINA_UNUSED, Evas_Object *obj, void *event_info EINA_UNUSED)
{
   //delete the event rect
   evas_object_del(data);
   if (_ui.popup_toolbar == obj)
     _ui.popup_toolbar = NULL;
}

void
extra_ui_show_popup_toolbar(Evas_Object *content)
{
    Evas_Object *tooltip, *rect;

    if (_ui.popup_toolbar)
      {
         evas_object_del(_ui.popup_toolbar);
         _ui.popup_toolbar = NULL;
      }

    if (!content) return;

    tooltip = elm_popup_add(_ui.win);
    elm_popup_allow_events_set(tooltip, EINA_TRUE);
    elm_popup_orient_set(tooltip, ELM_POPUP_ORIENT_TOP);
    evas_object_smart_callback_add(tooltip, "dismissed", _dismissed, tooltip);
    elm_object_content_set(tooltip, content);
    evas_object_show(tooltip);

    rect = evas_object_rectangle_add(evas_object_evas_get(_ui.win));
    evas_object_repeat_events_set(rect, EINA_TRUE);
    evas_object_size_hint_weight_set(rect, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    evas_object_size_hint_align_set(rect, EVAS_HINT_FILL, EVAS_HINT_FILL);
    evas_object_color_set(rect, 0, 0, 0, 0);
    evas_object_event_callback_add(rect, EVAS_CALLBACK_MOUSE_DOWN, _popup_block_click_cb, tooltip);
    elm_win_resize_object_add(_ui.win, rect);
    evas_object_show(rect);

    evas_object_event_callback_add(tooltip, EVAS_CALLBACK_DEL, _popup_del_cb, rect);

    _ui.popup_toolbar = tooltip;
}

static void
_detail_preview_del(void *data EINA_UNUSED, Evas *e EINA_UNUSED, Evas_Object *obj, void *event_info)
{
   Evas_Object *content;
   Evas_Event_Mouse_Up *up = event_info;
   Eina_Rectangle box;

   content = elm_object_content_get(obj);

   evas_object_geometry_get(content, &box.x, &box.y, &box.w, &box.h);

   if (eina_rectangle_coords_inside(&box, up->output.x , up->output.y)) return;

   evas_object_del(obj);
}

void
extra_ui_details_window_show(Evas_Object *content)
{
   Evas_Object *o;

   o = elm_win_inwin_add(_ui.win);
   evas_object_size_hint_weight_set(o, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
   evas_object_size_hint_align_set(o, EVAS_HINT_FILL, EVAS_HINT_FILL);
   evas_object_event_callback_add(o, EVAS_CALLBACK_MOUSE_UP, _detail_preview_del, o);
   evas_object_event_callback_add(o, EVAS_CALLBACK_KEY_DOWN, _key_down_cb, o);
   elm_win_inwin_activate(o);

   elm_object_content_set(o, content);

   elm_object_focus_set(content, EINA_TRUE);
}


static void
_fullscreen(void *data, Evas *e EINA_UNUSED, Evas_Object *obj EINA_UNUSED, void *event_info EINA_UNUSED)
{
   extra_ui_fullscreen_preview(data);
}


static void
_link(void *data, Evas *e EINA_UNUSED, Evas_Object *obj EINA_UNUSED, void *event_info EINA_UNUSED)
{
   Extra_Base_Object *ob = data;
   Eina_Strbuf *buf;

   buf = eina_strbuf_new();
   eina_strbuf_append_printf(buf, "xdg-open %s", ob->source);

   ecore_exe_run(eina_strbuf_release(buf), NULL);
}

Evas_Object*
extra_ui_base_object_detail(void *real_obj, Extra_Base_Object *obj, Extra_Ui_Small_Preview_Accessor acc, Evas_Object *par)
{
   Evas_Object *o, *table;
   Eina_Strbuf *buf;

   table = o = elm_table_add(par);
   evas_object_size_hint_weight_set(o, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
   evas_object_size_hint_align_set(o, EVAS_HINT_FILL, EVAS_HINT_FILL);
   evas_object_show(o);

   buf = eina_strbuf_new();
   eina_strbuf_append_printf(buf, "<title>%s</title>", obj->name);

   o = elm_label_add(table);
   elm_object_text_set(o, eina_strbuf_release(buf));
   evas_object_size_hint_weight_set(o, EVAS_HINT_EXPAND, 0.0);
   evas_object_size_hint_align_set(o, 0.0, EVAS_HINT_FILL);
   evas_object_show(o);
   elm_table_pack(table, o, 0, 0, 1, 1);

   o = extra_ui_small_preview_new(acc, table, obj);
   evas_object_event_callback_add(o, EVAS_CALLBACK_MOUSE_UP, _fullscreen, acc.preview_get(real_obj));
   evas_object_size_hint_weight_set(o, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
   evas_object_size_hint_align_set(o, EVAS_HINT_FILL, EVAS_HINT_FILL);
   evas_object_show(o);
   elm_table_pack(table, o, 0, 1, 2, 1);

   buf = eina_strbuf_new();
   eina_strbuf_append_printf(buf, "<link>%s</link>", obj->author);

   o = elm_entry_add(table);
   elm_object_text_set(o, eina_strbuf_release(buf));
   elm_entry_editable_set(o, EINA_FALSE);
   evas_object_size_hint_weight_set(o, EVAS_HINT_EXPAND, 0.0);
   evas_object_size_hint_align_set(o, EVAS_HINT_FILL, 0.0);
   evas_object_show(o);
   elm_table_pack(table, o, 0, 2, 1, 1);

   if (obj->source)
     {
        buf = eina_strbuf_new();
        eina_strbuf_append_printf(buf, "<link>%s</link>", _("Source"));

        o = elm_entry_add(table);
        evas_object_event_callback_add(o, EVAS_CALLBACK_MOUSE_UP, _link, obj);
        elm_entry_editable_set(o, EINA_FALSE);
        elm_object_text_set(o, eina_strbuf_release(buf));
        evas_object_size_hint_weight_set(o, EVAS_HINT_EXPAND, 0.0);
        evas_object_size_hint_align_set(o, EVAS_HINT_FILL, 0.0);
        evas_object_show(o);
        elm_table_pack(table, o, 1, 2, 1, 1);
     }

   return table;
}

typedef struct {
   Extra_Ui_Site_Template temp;
   Elm_Gengrid_Item_Class *class;
   Evas_Object *grid;
} Extra_Ui_Site;

static void
_selector_hide(void *data EINA_UNUSED, Evas *e EINA_UNUSED, Evas_Object *obj EINA_UNUSED, void *event_info EINA_UNUSED)
{
   if (_ui.popup_toolbar)
     {
        evas_object_del(_ui.popup_toolbar);
        _ui.popup_toolbar = NULL;
     }
}

Evas_Object*
extra_ui_site_add(Extra_Ui_Site_Template *site_temp)
{
   Extra_Ui_Site *site = calloc(1, sizeof(Extra_Ui_Site));
   Evas_Object *o;

   memcpy(&site->temp, site_temp, sizeof(Extra_Ui_Site_Template));

   site->class = elm_gengrid_item_class_new();
   site->class->item_style = site_temp->item_style;
   site->class->func.content_get = site->temp.content;
   site->class->func.text_get = site->temp.text;

   site->grid = o = elm_gengrid_add(_ui.win);
   elm_gengrid_select_mode_set(o, ELM_OBJECT_SELECT_MODE_ALWAYS);
   elm_gengrid_multi_select_set(o, EINA_FALSE);
   evas_object_size_hint_weight_set(o, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
   evas_object_size_hint_align_set(o, EVAS_HINT_FILL, EVAS_HINT_FILL);
   elm_gengrid_item_size_set(o, 300, 300);
   evas_object_smart_callback_add(o, "selected", site->temp.selected, _ui.win);
   evas_object_show(o);
   evas_object_data_set(o, "__site_data", site);
   evas_object_event_callback_add(o, EVAS_CALLBACK_HIDE, _selector_hide, NULL);

   extra_ui_site_refill(o);

   return o;
}

void
extra_ui_site_refill(Evas_Object *site)
{
   Extra_Ui_Site *pd = evas_object_data_get(site, "__site_data");
   Eina_List *n, *lst;
   void *data;

   EINA_SAFETY_ON_NULL_RETURN(pd);

   lst = pd->temp.list();

   EINA_LIST_FOREACH(lst, n, data)
     {
        elm_gengrid_item_append(pd->grid, pd->class, data, NULL, NULL);
     }
   elm_gengrid_item_bring_in(elm_gengrid_first_item_get(pd->grid), ELM_GENGRID_ITEM_SCROLLTO_TOP);
}
