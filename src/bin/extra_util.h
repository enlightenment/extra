#ifndef EXTRA_UTIL_H
#define EXTRA_UTIL_H

#include "../lib/extra.h"

/* Typedefs for later use */
typedef struct _Extra_Ui_Small_Preview_Accessor Extra_Ui_Small_Preview_Accessor;
typedef struct _Extra_Ui_Site_Template Extra_Ui_Site_Template;

/*
 * General purpose helper functions that displays dialogs / windows / and visual representation
 */

/**
 * Ask if the given theme should be displayed as default
 *
 * @param theme the extra theme to ask for
 */
void            extra_ui_theme_ask_for_default(Extra_Theme *theme);

/**
 * Create a popup that shows a popup with a progressbar and a label
 *
 * @param title the title to display in the popup
 * @param down_cb once the progress hit 100% this function is called
 * @param data the data to pass to down_cb
 *
 * @return a extra_progress you can pass this value to other api functions, the
           progress of this function will then be directly displayed in this object
 */
Extra_Progress* extra_ui_progress_popup_show(const char *title, Extra_Done_Cb done, void *data);

/**
 * Fill a Evas_Object with the data passed in by obj
 *
 * @param real_obj the object that gets passed to the accessor functions
 * @param obj the object that specifies the base data
 * @param acc the accessor to get preview and download path from a real_obj
 * @param parent the parent to use when creating the objects
 *
 * @return A Elm_Table that contains contents from
 *
 */
Evas_Object* extra_ui_base_object_detail(void *real_obj, Extra_Base_Object *obj, Extra_Ui_Small_Preview_Accessor acc, Evas_Object *par);

/**
 * Display a inwin in the extra window with the given content
 *
 * @param content the content to display in the inwin
 *
 */
void extra_ui_details_window_show(Evas_Object *content);

/**
 * Preview a path as fullscreen window on the desktop
 *
 * @param path the path to load the image from
 */
void extra_ui_fullscreen_preview(char *path);

/**
 * Popups a toolbar at the top center of the window with the specified content
 *
 * @param content the content to display
 *
 */
void extra_ui_show_popup_toolbar(Evas_Object *content);

/*
 * Util function to get the state of the current selected item reapplied
 *
 * @param grid a Elm_Gengrid
 */
void gengrid_reapply_state(Evas_Object *grid);


/*
 * Helperfunctions to create a visually representation of a preview or a progressbar
 * that displays the progress on downloading the preview
 */

/*
 * If the function returns NULL download is called
 */
typedef char *(Extra_ui_preview_get)(void *obj);
typedef Extra_Request *(Extra_ui_preview_download)(Extra_Progress *progress, void *obj);
struct _Extra_Ui_Small_Preview_Accessor{
    Extra_ui_preview_get *preview_get;
    Extra_ui_preview_download *preview_download;
} ;

/**
 * Create a object that previews the visualy component of a object
 * If a object is not ready to be displayed the data is downloaded, or the progressbar is displayed
 *
 * @param acc a struct that defines how to get the preview and download function out of data
 * @param obj the parent to use
 * @param data the data to pass to the functions of the acc functions
 *
 * @return a evas image that displayes the previews from the data ptr
 */
Evas_Object* extra_ui_small_preview_new(Extra_Ui_Small_Preview_Accessor acc, Evas_Object *par, void *data);

/**
 * Helper functions to define a site
 *
 * A site is just a gengrid that gets filled with the items returned by the list function pointer
 */
struct _Extra_Ui_Site_Template{
   Eina_List* (*list)(void);
   const char *item_style;
   Elm_Gen_Item_Content_Get_Cb content;
   Elm_Gen_Item_Text_Get_Cb text;
   Evas_Smart_Cb selected;
};

/**
 * Create a Evas Object out of a site template
 *
 * @param site_temp the content is later copied into the internal struct and
                    does not need to be present after this call
 */
Evas_Object* extra_ui_site_add(Extra_Ui_Site_Template *site_temp);

/*
 * Call again the list function to renew all items in the previously created site.
 *
 * @param site the evas object, returned by extra_ui_site_add
 *
 */
void extra_ui_site_refill(Evas_Object *site);

#endif
