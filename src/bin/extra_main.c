#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

/* NOTE: Respecting header order is important for portability.
 * Always put system first, then EFL, then your public header,
 * and finally your private one. */

#include <Ecore_Getopt.h>
#include <Elementary.h>
#include <Elementary_Cursor.h>

#include "gettext.h"

#include "extra_private.h"

#define COPYRIGHT "Copyright © 2016 Andy Williams <andy@andywilliams.me> and various contributors (see AUTHORS)."

Eina_List *_theme_list = NULL;
Eina_List *_wallpaper_list = NULL;

Ui _ui;

static void
_extra_win_del(void *data EINA_UNUSED, Evas_Object *obj EINA_UNUSED, void *event_info EINA_UNUSED)
{
   elm_exit();
}

static void
_item_changed(void *data EINA_UNUSED, Evas_Object *obj, void *event_info EINA_UNUSED)
{
   Elm_Segment_Control_Item *it =
    elm_segment_control_item_selected_get(obj);
   Evas_Object *new = elm_object_item_data_get(it);

   evas_object_hide(_ui.wallpaper_selector);
   evas_object_hide(_ui.theme_selector);

   evas_object_show(new);
}

static void
_extra_win_sync_done_cb(void *data EINA_UNUSED)
{
   Elm_Table *table, *segcontrol, *ic;
   Elm_Object_Item *it;
   char buf[256], buf1[256];

   table = elm_table_add(_ui.win);
   evas_object_size_hint_weight_set(table, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
   evas_object_size_hint_align_set(table, EVAS_HINT_FILL, EVAS_HINT_FILL);
   evas_object_show(table);

   _ui.wallpaper_selector = extra_ui_site_add(&wallpaper_site);
   elm_table_pack(table, _ui.wallpaper_selector, 0, 1, 1, 1);

   _ui.theme_selector = extra_ui_site_add(&theme_site);
   elm_table_pack(table, _ui.theme_selector, 0, 1, 1, 1);

   segcontrol = elm_segment_control_add(table);
   evas_object_size_hint_weight_set(segcontrol, EVAS_HINT_EXPAND, 0.0);
   evas_object_size_hint_align_set(segcontrol, EVAS_HINT_FILL, 0.0);
   elm_table_pack(table, segcontrol, 0, 0, 1, 1);
   evas_object_smart_callback_add(segcontrol, "changed", _item_changed, NULL);
   evas_object_show(segcontrol);

   snprintf(buf, sizeof(buf), "%s [%i]", _("Themes"), eina_list_count(_theme_list));
   snprintf(buf1, sizeof(buf1), "%s [%i]", _("wallpapers"), eina_list_count(_wallpaper_list));

#define IC_ADD(ic_txt, txt, obj) \
   ic = elm_icon_add(segcontrol); \
   elm_icon_standard_set(ic, ic_txt); \
   it = elm_segment_control_item_add(segcontrol, ic, txt); \
   elm_object_item_data_set(it, obj); \

   IC_ADD("preferences-desktop-theme", buf, _ui.theme_selector);
   IC_ADD("preferences-desktop-wallpaper", buf1, _ui.wallpaper_selector);

#undef IC_ADD

   elm_segment_control_item_selected_set(elm_segment_control_item_get(segcontrol, 0), EINA_TRUE);

   elm_win_resize_object_add(_ui.win, table);
}

static void
extra_win_sync(void)
{
   Extra_Progress *progress;

   progress = extra_ui_progress_popup_show(_("Updating themes and wallpapers"), _extra_win_sync_done_cb, NULL);
   extra_sync(progress);
}

static Evas_Object *
extra_win_setup(void)
{

   _ui.win = elm_win_util_standard_add("main", "Extra!");
   if (!_ui.win) return NULL;

   elm_win_focus_highlight_enabled_set(_ui.win, EINA_TRUE);
   evas_object_smart_callback_add(_ui.win, "delete,request", _extra_win_del, NULL);

   evas_object_resize(_ui.win, 420 * elm_config_scale_get(),
                           280 * elm_config_scale_get());

   elm_win_center(_ui.win, EINA_TRUE, EINA_TRUE);

   evas_object_show(_ui.win);

   return _ui.win;
}

static const Ecore_Getopt optdesc = {
  "extra",
  "%prog [options]",
  PACKAGE_VERSION,
  COPYRIGHT,
  "3 clause BSD license",
  "An Enlightenment theme and plugin browser",
  0,
  {
    ECORE_GETOPT_STORE_TRUE('s', "skip-sync",
      "Skip the initial theme sync when loading UI"),
    ECORE_GETOPT_LICENSE('L', "license"),
    ECORE_GETOPT_COPYRIGHT('C', "copyright"),
    ECORE_GETOPT_VERSION('V', "version"),
    ECORE_GETOPT_HELP('h', "help"),
    ECORE_GETOPT_SENTINEL
  }
};

EAPI_MAIN int
elm_main(int argc EINA_UNUSED, char **argv EINA_UNUSED)
{
   Evas_Object *win;
   int args;
   Eina_Bool quit_option = EINA_FALSE, skip_option = EINA_FALSE;

   Ecore_Getopt_Value values[] = {
     ECORE_GETOPT_VALUE_BOOL(skip_option),
     ECORE_GETOPT_VALUE_BOOL(quit_option),
     ECORE_GETOPT_VALUE_BOOL(quit_option),
     ECORE_GETOPT_VALUE_BOOL(quit_option),
     ECORE_GETOPT_VALUE_BOOL(quit_option),
     ECORE_GETOPT_VALUE_NONE
   };

#if ENABLE_NLS
   setlocale(LC_ALL, "");
   bindtextdomain(PACKAGE, LOCALEDIR);
   bind_textdomain_codeset(PACKAGE, "UTF-8");
   textdomain(PACKAGE);
#endif

   extra_init();

   args = ecore_getopt_parse(&optdesc, values, argc, argv);
   if (args < 0)
     {
	EINA_LOG_CRIT("Could not parse arguments.");
	goto end;
     }
   else if (quit_option)
     {
	goto end;
     }

   elm_app_info_set(elm_main, "extra", "images/extra.svg");

   if (!(win = extra_win_setup()))
     goto end;

   if (skip_option)
     _extra_win_sync_done_cb(NULL);
   else
     extra_win_sync();

   elm_run();

 end:
   extra_shutdown();
   elm_shutdown();

   return 0;
}
ELM_MAIN()
