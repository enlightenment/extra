#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include <stddef.h>
#include <sys/stat.h>

#include "extra.h"
#include "jsmn/jsmn.h"
#include "extra_private.h"


static Eina_Strbuf*
_get_json_syntax(Eina_Strbuf *buf, jsmntok_t *tok)
{
   return eina_strbuf_substr_get(buf, tok->start, tok->end - tok->start);
}

static int
_string_tuple_get(Eina_Strbuf *buf, jsmntok_t *array, int i, Eina_Strbuf **name, Eina_Strbuf **value)
{
   if (array[i].type != JSMN_STRING || array[i].size != 1)
     {
        printf("expected string type with children\n");
        return 0;
     }

   *name = _get_json_syntax(buf, &array[i]);

   if ((array[i + 1].type != JSMN_STRING && array[i + 1].type != JSMN_PRIMITIVE) || array[i + 1].size != 0)
     {
        printf("Expected string type without children\n");
        return 0;
     }

   *value = _get_json_syntax(buf, &array[i + 1]);

   return 2;
}

//get the value of a instance described with the given part
static Eina_Strbuf*
_field_get(void *instance, Extra_Instance_Part *part)
{
   return *((Eina_Strbuf**)(instance + part->size));
}

//set the value of a instance described with the given part
static void
_field_set(void *instance, Extra_Instance_Part *part, Eina_Strbuf *data)
{
   *((Eina_Strbuf**)(instance + part->size)) = data;
}

void
extra_json_list_part_free(Extra_Json_To_List_Template *tmp, void *instance)
{
   for (unsigned int j = 0; j < tmp->tuples_size; ++j)
     {
        Eina_Strbuf *c;
        c = _field_get(instance, &tmp->tuples[j]);
        if (c)
          eina_strbuf_free(c);
     }
   free(instance);
}

static int
_instance_new(Extra_Json_To_List_Template *tmp, Eina_Strbuf *buf, jsmntok_t parts[], void **ret)
{
   void *instance = calloc(1, tmp->structsize);
   int c = 0;

   c++;
   *ret = NULL;

   for (int i = 0; i < parts[0].size; ++i)
     {
        Eina_Strbuf *key = NULL, *data = NULL;
        Eina_Bool success = EINA_FALSE;
        int j;
        const char *v;
        j = _string_tuple_get(buf, parts, c, &key, &data);

        if (!j) return 0;
        c += j;

        v = eina_strbuf_string_get(key);

        //map to the list of write tuples
        for (unsigned int j = 0; j < tmp->tuples_size; ++j)
          {
            if (!strcmp(tmp->tuples[j].name, v))
              {
                 if (_field_get(instance, &tmp->tuples[j]))
                   {
                      Eina_Strbuf *buf = NULL;
                      buf = _get_json_syntax(buf, &parts[i]);
                      //double setting the same field
                      ERR("In instance %p, field %s was set twice!\n%s\n", instance, tmp->tuples[i].name, eina_strbuf_string_get(buf));
                      eina_strbuf_free(buf);

                      extra_json_list_part_free(tmp, instance);
                      instance = NULL;
                      return 0;
                   }
                 _field_set(instance, &tmp->tuples[j], data);
                 success = EINA_TRUE;
                 break;
              }
          }

        //silently ignore data key pairs that are not found
        //this enables changing the protocol without killing the app
        if (!success)
          eina_strbuf_free(data);
        eina_strbuf_free(key);
     }

   //validate that the new instance has all required fields
   for (unsigned int i = 0; i < tmp->tuples_size; ++i)
     {
        if (tmp->tuples[i].required && !_field_get(instance, &tmp->tuples[i]))
          {
             ERR("In instance %p, required field %s was not set!\n", instance, tmp->tuples[i].name);
             //free all set values
             extra_json_list_part_free(tmp, instance);
             instance = NULL;
             break;
          }
     }


   *ret = instance;
   return c;
}

Eina_List*
extra_json_to_list(Extra_Json_To_List_Template *tmp, Eina_Strbuf *buf)
{
   jsmn_parser parser;
   Eina_List *ret = NULL;
   jsmntok_t parts[201];
   const char *string;
   int n = 0;
   int c = 0;

   jsmn_init(&parser);

   //parse the json text
   string = eina_strbuf_string_get(buf);
   n = jsmn_parse(&parser, string, strlen(string), parts, sizeof(parts));

   if (n == 0) return NULL;

   if (parts[0].type != JSMN_OBJECT)
     {
        printf("Root node should be a object");
        return NULL;
     }
   c ++;

   for (int i = 0; i < parts[0].size; ++i)
     {
        void *instance;
        int j;

        if (parts[c].type != JSMN_STRING || parts[c].size != 1)
          {
             printf("Expected String type with one child\n");
             return NULL;
          }
        c ++;

        if (parts[c].type != JSMN_OBJECT || parts[c].size <= 0)
          {
             printf("Expected Object type with more than 0 children\n");
             return NULL;
          }

        //create a instance for that object
        j = _instance_new(tmp, buf, parts + c, &instance);
        if (!j) return NULL;
        c += j;

        //add this to the list of instances
        if (instance)
          ret = eina_list_append(ret, instance);

     }
   return ret;
}

typedef struct {
   Extra_Request req;
   Extra_Request **clean_up;
   char *from;
   char *to;
   Ecore_File_Download_Job *cache;
   Ecore_File_Download_Job *full;
} Extra_Download_Job;

static void
_download_clean_up(Extra_Download_Job *job)
{
   if (job->clean_up)
     *job->clean_up = NULL;
}

static void
_download_job_free(Extra_Download_Job *job)
{
   _download_clean_up(job);

   free(job->from);
   free(job->to);
   free(job);
}

static void
_download_complete_cb(void *data, const char *file EINA_UNUSED, int status EINA_UNUSED)
{
   Extra_Download_Job *job = data;

   if (status != 200)
     ecore_file_remove(file);

   _download_clean_up(job);

   if (job->req.progress.done_cb)
     job->req.progress.done_cb(job->req.progress.data);

   _download_job_free(job);
}

static int
_download_progress_cb(void *data, const char *file EINA_UNUSED,
                      long int dltotal EINA_UNUSED, long int dlnow EINA_UNUSED,
                      long int ultotal EINA_UNUSED, long int ulnow EINA_UNUSED)
{
   Extra_Download_Job *job = data;
   double percent = 0.f;

   if (dlnow > 0.f)
     percent = ((double)(double)dlnow / (double)dltotal);


   if (job->req.progress.progress_cb)
     job->req.progress.progress_cb(job->req.progress.data, percent);

   return ECORE_FILE_PROGRESS_CONTINUE;
}

static int
_download_check_progress_cb(void *data EINA_UNUSED, const char *file,
                      long int dltotal EINA_UNUSED, long int dlnow EINA_UNUSED,
                      long int ultotal EINA_UNUSED, long int ulnow EINA_UNUSED)
{
   Extra_Download_Job *job;
   struct stat st;

   job = data;

   if (dltotal == 0)
     return ECORE_FILE_PROGRESS_CONTINUE;

   if (!!stat(job->to, &st))
     {
        ERR("Failed to fetch stat from %s", file);
        goto end;
     }

   if (st.st_size != dltotal)
     {
        //the size of the image has changed ... remove and download again
        ecore_file_remove(job->to);
        ecore_file_download(job->from, job->to,
          _download_complete_cb,
          _download_progress_cb,
          job, &job->full);
        goto end;
     }
   else
     {
        INF("Everything is ok %s %ld\n", file, dltotal);
        ecore_file_download_abort(job->cache);
        _download_job_free(job);
     }
   return ECORE_CALLBACK_CANCEL;

end:
   ecore_file_download_abort(job->cache);
   job->cache = NULL;
   return ECORE_CALLBACK_CANCEL;
}

static Extra_Download_Job*
_extra_file_job_new(Extra_Progress *progress, const char *from, const char *to, Extra_Request **req)
{
   Extra_Download_Job *job;

   job = calloc(1, sizeof(Extra_Download_Job));
   job->req.progress = *progress;
   job->clean_up = req;
   job->to = strdup(to);
   job->from = strdup(from);

   if (req)
     *req = &job->req;

   return job;
}

void
extra_file_cache_download(Extra_Progress *progress, const char *from, const char *to, Extra_Request **req)
{
   Extra_Download_Job *job = _extra_file_job_new(progress, from, to, req);

   char path[PATH_MAX], *dir;
   const char *file;

   file = ecore_file_file_get(to);
   dir = ecore_file_dir_get(to);

   snprintf(path, sizeof(path), "%s/cache-%s", dir, file);
   ecore_file_remove(path);
   ecore_file_download(from, path,
     NULL,
     _download_check_progress_cb,
     job, &job->cache);

   free(dir);
}

void
extra_file_download(Extra_Progress *progress, const char *from, const char *to, Extra_Request **req)
{
   Extra_Download_Job *job = _extra_file_job_new(progress, from, to, req);

   ecore_file_download(from, to,
     _download_complete_cb,
     _download_progress_cb,
     job, &job->full);

 }
