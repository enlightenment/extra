#ifndef EXTRA_PRIVATE_H
# define EXTRA_PRIVATE_H

#define HOSTNAME "extra.enlightenment.org"

extern int _extra_lib_log_dom;

#ifdef ERR
# undef ERR
#endif
#define ERR(...) EINA_LOG_DOM_ERR(_extra_lib_log_dom, __VA_ARGS__)
#ifdef INF
# undef INF
#endif
#define INF(...) EINA_LOG_DOM_INFO(_extra_lib_log_dom, __VA_ARGS__)
#ifdef WRN
# undef WRN
#endif
#define WRN(...) EINA_LOG_DOM_WARN(_extra_lib_log_dom, __VA_ARGS__)
#ifdef CRIT
# undef CRIT
#endif
#define CRIT(...) EINA_LOG_DOM_CRIT(_extra_lib_log_dom, __VA_ARGS__)
#ifdef DBG
# undef DBG
#endif
#define DBG(...) EINA_LOG_DOM_DBG(_extra_lib_log_dom, __VA_ARGS__)

typedef struct {
   const char *name;
   size_t size; // this pointer constists out of instance + ptr
   Eina_Bool required;
} Extra_Instance_Part;

typedef struct {
   size_t structsize;
   Extra_Instance_Part *tuples;
   size_t tuples_size;
} Extra_Json_To_List_Template;

struct _Extra_Request
{
   Eina_Bool muted;
   Extra_Progress progress;
};

extern Eina_List *_theme_list;
extern Eina_List *_wallpaper_list;

#define EXTRA_STATE_DOWNLOAD_IN_PROGRESS 2


#define EXTRA_JSON_STRUCT_FIELD(name, str, val, required) { name , ((size_t)&(((str *)0)->val)), required }

#define EXTRA_JSON_TO_LIST_TEMPLATE_INIT(v, str, ...) \
Extra_Instance_Part part[] = {__VA_ARGS__}; \
Extra_Json_To_List_Template v = { \
    sizeof(str), \
    part, \
    sizeof(part)/sizeof(Extra_Instance_Part) \
}

Eina_List* extra_json_to_list(Extra_Json_To_List_Template *tmp, Eina_Strbuf *buf);
void extra_json_list_part_free(Extra_Json_To_List_Template *tmp, void *data);

void extra_file_download(Extra_Progress *progress, const char *from, const char *to, Extra_Request **req);
void extra_file_cache_download(Extra_Progress *progress, const char *from, const char *to, Extra_Request **req);
#endif
