#ifndef EXTRA_H_
# define EXTRA_H_

#include <Elementary.h>

#ifdef EAPI
# undef EAPI
#endif

#ifdef _WIN32
# ifdef EFL_EXTRA_BUILD
#  ifdef DLL_EXPORT
#   define EAPI __declspec(dllexport)
#  else
#   define EAPI
#  endif /* ! DLL_EXPORT */
# else
#  define EAPI __declspec(dllimport)
# endif /* ! EFL_EXTRA_BUILD */
#else
# ifdef __GNUC__
#  if __GNUC__ >= 4
#   define EAPI __attribute__ ((visibility("default")))
#  else
#   define EAPI
#  endif
# else
#  define EAPI
# endif
#endif /* ! _WIN32 */

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @file
 * @brief These routines are used for extra library interaction.
 */

typedef void (*Extra_Progress_Cb)(void *data, double progress);
typedef void (*Extra_Done_Cb)(void *data);

typedef struct _Extra_Base_Object
{
   const char *id;
   const char *name;
   const char *author;
   const char *source;
   int version;

} Extra_Base_Object;

typedef struct _Extra_Theme
{
   Extra_Base_Object obj;
   const char *description;
} Extra_Theme;

typedef struct _Extra_Wallpaper
{
   Extra_Base_Object obj;
} Extra_Wallpaper;

typedef struct _Extra_Progress
{
   Extra_Progress_Cb progress_cb;
   Extra_Done_Cb done_cb;
   void *data;
} Extra_Progress;

typedef struct _Extra_Request Extra_Request;

/**
 * @brief Init / shutdown functions.
 * @defgroup Init  Init / Shutdown
 *
 * @{
 *
 * Functions of obligatory usage, handling proper initialization
 * and shutdown routines.
 *
 * Before the usage of any other function, extra should be properly
 * initialized with @ref extra_init() and the last call to extra's
 * functions should be @ref extra_shutdown(), so everything will
 * be correctly freed.
 *
 * extra logs everything with Eina Log, using the "extra" log domain.
 *
 */

/**
 * Initialize extra.
 *
 * Initializes extra, its dependencies and modules. Should be the first
 * function of extra to be called.
 *
 * @return The init counter value.
 *
 * @see extra_shutdown().
 *
 * @ingroup Init
 */
EAPI int extra_init(void);

/**
 * Shutdown extra
 *
 * Shutdown extra. If init count reaches 0, all the internal structures will
 * be freed. Any extra library call after this point will leads to an error.
 *
 * @return extra's init counter value.
 *
 * @see extra_init().
 *
 * @ingroup Init
 */
EAPI int extra_shutdown(void);

/**
 * @}
 */

/**
 * @brief Main group API that wont do anything
 * @defgroup Storage Storage
 *
 * @{
 *
 * @brief Sync the library with the extra_server to get the theme list.
 *
 * @ingroup Storage
 */
EAPI void extra_sync(Extra_Progress *progress);

/**
 * @}
 */

/**
 * @brief Theme management functions
 * @defgroup Themes Themes
 *
 * @{
 * @brief Get a list of all the themes that are available.
 *
 * @see extra_sync
 *
 * @ingroup Themes
 */
EAPI Eina_List *extra_themes_list(void);

EAPI Extra_Theme *extra_theme_get(const char *id);

EAPI Eina_Bool extra_theme_installed(Extra_Theme *theme);

EAPI char *extra_theme_install_path_get(Extra_Theme *theme);

EAPI char *extra_theme_preview_get(Extra_Theme *theme);
EAPI Extra_Request* extra_theme_preview_download(Extra_Progress *progress, Extra_Theme *theme);

EAPI char *extra_theme_download_url_get(Extra_Theme *theme);

/**
 * @brief Download the specified theme.
 *
 * @ingroup Themes
 */
EAPI Extra_Request* extra_theme_download(Extra_Progress *progress, Extra_Theme *theme);

/**
 * @brief Set the theme as default and restart e
 *
 * @ingroup Themes
 */
EAPI void extra_theme_use(Extra_Theme *t);
EAPI Eina_Bool extra_theme_default_get(Extra_Theme *t);
EAPI Eina_Bool extra_theme_installed_old(Extra_Theme *t);

EAPI void extra_theme_reset(void);
/**
 * @}
 */

EAPI Eina_List* extra_wallpapers_list(void);
EAPI Extra_Wallpaper* extra_wallpaper_get(const char *id);

EAPI Eina_Bool extra_wallpaper_installed(Extra_Wallpaper *b);
EAPI Extra_Request* extra_wallpaper_download(Extra_Progress *progress, Extra_Wallpaper *b);
EAPI void extra_wallpaper_delete(Extra_Wallpaper *b);

EAPI char* extra_wallpaper_preview_get(Extra_Wallpaper *wallpaper);
EAPI Extra_Request* extra_wallpaper_preview_download(Extra_Progress *progress, Extra_Wallpaper *wallpaper);

EAPI void extra_request_mute(Extra_Request *req);

#ifdef __cplusplus
}
#endif

#endif /* EXTRA_H_ */
